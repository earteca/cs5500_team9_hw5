def main():

    lst = [1,2,34,5,6,7,8]
    for i in lst:
        print("hello world")

    for i in lst:
        if (i %2 ==0):
            print("multiple of 2")
        else:
            print("it's not yet so we'll add 1 to it so it becomes", i+1)
        print("and that's my boring program")

main()