package NEU.antlr;

/**
 *
 * Hello world!
 *
 */
import org.antlr.v4.runtime.*;
import java.io.IOException;



public class App {

    /**
     * This is an LCS method to return the LCS of two strings. The two strings here are the
     * two trees being compared (which represent the tree form of the two files being compared)
     * The method works using DP by creating a 2d array and then comparing each character at each
     * position. If a character is equal to the other string's index, it updates the 2d array
     * with the necessary index markers to eventually be returned at the end. Runs in n^2 time.
     *
     * @param tree1 the first tree
     * @param tree2 the second tree
     * @return
     */
    public String longestCommonSubstring(String tree1, String tree2) {

        int len1 = tree1.length();
        int len2 = tree2.length();
        int finalIndexMark;

        int currentMaxSubLength = 0;

        int[][] charMatrix = new int[len1][len2];
        finalIndexMark = -1;
        for(int i =0; i<len1; i++) {
            for(int j = 0; j<len2; j++){
                if(tree1.charAt(i) == tree2.charAt(j)){
                    if(i == 0 || j == 0) {
                        charMatrix[i][j] = 1;
                    }
                    else {
                        charMatrix[i][j] = charMatrix[i - 1][j - 1] + 1;
                    }
                    if(currentMaxSubLength <charMatrix[i][j]) {
                        currentMaxSubLength = charMatrix[i][j];
                        finalIndexMark = i;
                    }
                    }
                }
            }

        //This finally just returns the substring between the relevant marked index spots.
        return tree1.substring(finalIndexMark-currentMaxSubLength + 1, finalIndexMark+1);
        }

    /**
     * This method gives a rudimentary output showing the amount of similarity that exists
     * from a strictly numerical point of view. The first iteration shown hered is very very
     * and no attention should be paid to whether or not this is a useful algorithm - it will
     * be refined later.
     *
     * @param similarityMeasure
     */
    public void showResults(int similarityMeasure){

        if(similarityMeasure > 20){

            System.out.println("Similarity between your programs is significant");
        }
        else if(similarityMeasure < 20 && similarityMeasure > 10){

            System.out.println("Similarity between your programs is moderate");
        }
        else if(similarityMeasure < 10){

            System.out.println("Similarity between your programs is small");
        }

    }



    /**
     * This method is central to the process of running the detector. It creates an AST from the
     * provided file which will enable us to use the tree for running the comparison algorithm.
     *
     * @param filename a string - this is the name of the file to be turned into an AST.
     * @return a string - this is the AST that has been produced from the method.
     */
    public String createTree(String filename) {

        CharStream charStream = null;


        try {
            //get the location of the file to parse
            charStream = CharStreams.fromFileName(filename);
        } catch (IOException e) {
            e.printStackTrace();
        }
        //creates the lexer
        Python3Lexer lexer = new Python3Lexer(charStream);
        //turns the lexer into a token stream
        CommonTokenStream commonTokenStream = new CommonTokenStream(lexer);
        //feeds the token stream into the parser
        NEU.antlr.Python3Parser parser = new NEU.antlr.Python3Parser(commonTokenStream);
        //creates the parsetree
        NEU.antlr.Python3Parser.File_inputContext inputTree = parser.file_input();
        // prints the tree
        System.out.println(inputTree.toStringTree(parser));
        NEU.antlr.Python3BaseVisitor visitor = new NEU.antlr.Python3BaseVisitor();
        inputTree.accept(visitor);
        return inputTree.toStringTree(parser);

    }


    public static void main(String[] args) throws IOException {

        CharStream charStream = null;


        try {
            //get the location of the file to parse
            charStream = CharStreams.fromFileName("/Users/fergusscott/Documents/Northeastern_Sem_4/MSD/PlagiarismDetector/hello.py");
        } catch (IOException e) {
            e.printStackTrace();
        }
        //creates the lexer
        Python3Lexer lexer = new Python3Lexer(charStream);
        //turns the lexer into a token stream
        CommonTokenStream commonTokenStream = new CommonTokenStream(lexer);
        //feeds the token stream into the parser
        NEU.antlr.Python3Parser parser = new NEU.antlr.Python3Parser(commonTokenStream);
        //creates the parsetree
        NEU.antlr.Python3Parser.File_inputContext inputTree = parser.file_input();
        // prints the tree
        System.out.println(inputTree.toStringTree(parser));
        NEU.antlr.Python3BaseVisitor visitor = new NEU.antlr.Python3BaseVisitor();
        inputTree.accept(visitor);

    }
}


//look into longest common subsequence
// to traverse tree use base visitor which is useful for counting
// the output is a string and you can
//store the output in a variable and start going through it with LCS
//potentially store all this code in a method like "getTree" or something
