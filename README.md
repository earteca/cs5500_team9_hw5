# Team-09-F19
team repo for Team-09-F19

At the moment this is the current state of affairs: 

The only file that has been readily and intentionally created by us is the file App.java.
This contains the bulk of the code that we need to run the program currently. We are yet to 
implement the algorithm that will give the reading of plagiarism detection. However, because
our project has been so stripped down, much of the code we may have generated for other purposes
such as front end, database interaction, is not needed. 

All other files in the program such as the Python parser, lexer, visitor, etc, were generated 
as a result of ANTLR construction. They are key to the success, but have not been created by a human
and as such, have zero documentation.

The python file Hello_antlr, is a basic file we've been using to just check that the antlr processes are working correctly. The 

The Project_Src folder is where all of the meaningful code is stored and will be where the rest of 
development occurs. 
